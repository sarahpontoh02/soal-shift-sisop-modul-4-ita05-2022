# Soal Shift Sistem Operasi Modul-4 ITA05
Anggota kelompok:
- Sarah Hanifah Pontoh - 5027201006
- Rama Muhammad Murshal - 5027201041
- Muhammad Rifqi Fernanda - 5027201050

# Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

## Soal 1

```c
int indexTanpaExt(char *path)
{
	int flag = 0;
	for (int i = strlen(path) - 1; i >= 0; i--)
	{
		if (path[i] == '.')
		{
			if (flag == 1)
				return i;
			else
				flag = 1;
		}
	}
	return strlen(path);
}

int indexSetelahSlash(char *path, int hasil)
{
	for (int i = 0; i < strlen(path); i++)
	{
		if (path[i] == '/')
			return i + 1;
	}
	return hasil;
}
```
Kedua fungsi di atas digunakan untuk membantu membaca direktori. `indexTanpaExt` digunakan untuk mengambil index tanpa ekstensi, sedangkan `indexSetelahSlash` digunakan untuk mengambil index setelah slash (`/`).

```c
void enkripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, 0);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}

void dekripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, idAkhir);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}

```
Kedua fungsi diatas digunakan untuk enkripsi dan dekripsi. `enkripsiAwalan1` melakukan enkripsi dengan mengambil index awal dan akhir dari path yang akan dienkripsi. Program mengiterasi dari index awal hingga akhir, dan melakukan enkripsi masing-masing karakter. Sementara untuk fungsi `dekripsiAwalan1`, dilakukan dekripsi karakter yang cara kerjanya sama seperti enkripsi namun untuk kebalikannya.

```c
void writeTheLog(char *nama, const char *from, const char *to)
{
	time_t rawtime;
	time(&rawtime);

	char textLog[1000];

	FILE *file;
	file = fopen(logPath, "a");

	if (strstr(to, awalan1)) sprintf(textLog, "RENAME\tterenkripsi\t%s\t-->\t%s\n", from, to);
	else sprintf(textLog, "RENAME\tterdecode\t%s\t-->\t%s\n", from, to);

	fputs(textLog, file);
	fclose(file);
	return;
}
```
Fungsi `writeTheLog` digunakan untuk membuat log. Fungsi ini dipanggil pada `xmp_rename` karena setiap dilakukan rename harus dibuat log.

### Kendala no.1

Masih error saat di-mouont, dan kelompok kami juga masih butuh belajar lebih dalam lagi untuk memahami FUSE.

## Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :

### Soal 2.a
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

### Pembahasan Soal 2.a

Variabel `keyVigen` yang digunakan untuk menyimpan string kunci untuk melakukan enkripsi vigenere:

```c
char keyVigen[100] = "INNUGANTENG";
```

Fungsi `vigen()`, digunakan untuk mengekripsi data yang dipass kedalamnya:

```c
char *vigen(char str[])
{
	int strLen = strlen(str), i, j, keyLen = strlen(keyVigen);
	char newKey[strLen];

	// generating new key
	for (i = 0, j = 0; i < strLen; ++i, ++j)
	{
		if (j == keyLen)
			j = 0;

		newKey[i] = keyVigen[j];
	}
	newKey[i] = '\0';

	char encrypted[100];
	// encryption
	for (i = 0; i < strLen; ++i)
		encrypted[i] = ((str[i] + newKey[i]) % 26) + 'A';

	encrypted[i] = '\0';

	char *res = encrypted;

	return res;
}
```

Fungsi `find_path()`, digunakan untuk mencari path absolute dari setiap file yang sedang ditinjau:

```c
char *find_path(const char *path)
{
	char fpath[2 * maxSize];
	bool flag, toMirror = 0;

	char *strMir;
	if (strcmp(path, "/")) // if path > /, mean if path contains /
	{
		strMir = strstr(path, "/IAN_");
		if (strMir)
		{
			// printf("Strmir past: %s \n", strMir);
			toMirror = 1;
			strMir++;
			// printf("Strmir new: %s \n", strMir);
		}
	}

	if (strcmp(path, "/") == 0)
	{
		sprintf(fpath, "%s", dirpath);
	}
	else if (toMirror)
	{
		char pathOrigin[maxSize] = "";
		char t[maxSize];

		strncpy(pathOrigin, path, strlen(path) - strlen(strMir)); // path origin only contain /
		// printf("Path origin: %s, path: %s \n", pathOrigin, path);
		strcpy(t, strMir);

		char *selectedFile;
		char *rest = t;

		flag = 0;

		while ((selectedFile = strtok_r(rest, "/", &rest)))
		{
			if (!flag)
			{
				strcat(pathOrigin, selectedFile);
				flag = 1;
				continue;
			}

			// printf("Path origin: %s, selectedFile: %s \n", pathOrigin, selectedFile);

			char checkType[2 * maxSize];
			sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
			strcat(pathOrigin, "/");
			// printf("pathOrigin: %s \n", pathOrigin);

			if (strlen(checkType) == strlen(path))
			{
				char pathFolder[2 * maxSize];
				sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

				DIR *dp = opendir(pathFolder);
				if (!dp)
				{
					char *ext;
					ext = strrchr(selectedFile, '.');

					char fileName[maxSize] = "";
					if (ext)
					{
						strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
						sprintf(fileName, "%s%s", vigen(fileName), ext);
					}
					else if (toMirror)
						strcpy(fileName, vigen(selectedFile));

					// printf("%s\n", fileName);
					strcat(pathOrigin, fileName);
				}
				else
					strcat(pathOrigin, vigen(selectedFile));
			}
			else
			{
				strcat(pathOrigin, vigen(selectedFile));
			}
		}
		sprintf(fpath, "%s%s", dirpath, pathOrigin);
	}
	else
		sprintf(fpath, "%s%s", dirpath, path);

	char *fpath_to_return = fpath;
	// printf("fpath_to_return: %s \n", fpath_to_return);
	return fpath_to_return;
}
```

Fungsi `xmp_readdir()`, digunakan untuk melakukan proses pembacaan directory saat dibuka:

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
	char fpath[maxSize];
	bool toMirror = strstr(path, "/IAN_");
	strcpy(fpath, find_path(path));

	int res = 0;
	DIR *dp;
	struct dirent *de;

	(void)offset;
	(void)fi;

	dp = opendir(fpath);
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL)
	{
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
		{
			res = (filler(buf, de->d_name, &st, 0));
		}
		else if (toMirror)
		{
			if (de->d_type & DT_DIR)
			{
				char temp[maxSize];
				strcpy(temp, de->d_name);

				res = (filler(buf, vigen(temp), &st, 0));
			}
			else
			{
				char *ext;
				ext = strrchr(de->d_name, '.');

				char fileName[maxSize] = "";
				if (ext)
				{
					strncpy(fileName, de->d_name, strlen(de->d_name) - strlen(ext));
					strcpy(fileName, vigen(fileName));
					strcat(fileName, ext);
				}
				else
				{
					strcpy(fileName, vigen(de->d_name));
				}
				res = (filler(buf, fileName, &st, 0));
			}
		}
		else
			res = (filler(buf, de->d_name, &st, 0));

		if (res != 0)
			break;
	}
	closedir(dp);
	return 0;
}
```

Fungsi `xmp_read()`, digunakan untuk membaca suatu file apabila dibuka:

```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	char fpath[maxSize];
	strcpy(fpath, find_path(path));

	int fd;
	int res;

	(void)fi;
	fd = open(fpath, O_RDONLY);
	if (fd == -1)
		return -errno;

	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	close(fd);
	return res;
}
```

Fungsi `xmp_getattr()`, digunakan untuk mendapatkan atribute dari file yang spesifik:

```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
	char fpath[maxSize];
	strcpy(fpath, find_path(path));

	int res;
	res = lstat(fpath, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}
```

Struct `fuse_operations` untuk meregistrasikan fungsi-fungsi yang ada:

```c
static struct fuse_operations operations = {
		.getattr = xmp_getattr,
		.readdir = xmp_readdir,
		.read = xmp_read,
};
```

Main function:

```c
int main(int argc, char *argv[])
{
	char *username = getenv("USER");

	sprintf(dirpath, "/home/%s", username);

	umask(0);

	return fuse_main(argc, argv, &operations, NULL);
}
```

### Kendala no.2

Masih sangat kesulitan untuk memahami cara kerja fuse untuk me-mount file system pada program meskipun sudah dijalankan di foreground.

## Soal 3

### Soal 3.a
