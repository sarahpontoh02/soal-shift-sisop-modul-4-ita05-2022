#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/time.h>
#define maxSize 1024


/*
char dirpath[maxSize];

char dir_list[256][256];
int curr_dir_idx = -1;

char files_list[256][256];
int curr_file_idx = -1;

char files_content[256][256];
int curr_file_content_idx = -1;
*/


static const char *dirPath = "/home/ubuntu/Documents";
static const char *logPath = "/home/ubuntu/Wibu.log";

char *awalan1 = "Animeku_";
int x = 0;

void writeTheLog(char *nama, const char *from, const char *to)
{
	time_t rawtime;
	time(&rawtime);

	char textLog[1000];

	FILE *file;
	file = fopen(logPath, "a");

	if (strstr(to, awalan1)) sprintf(textLog, "RENAME\tterenkripsi\t%s\t-->\t%s\n", from, to);
	else sprintf(textLog, "RENAME\tterdecode\t%s\t-->\t%s\n", from, to);

	fputs(textLog, file);
	fclose(file);
	return;
}

int indexTanpaExt(char *path)
{
	int flag = 0;
	for (int i = strlen(path) - 1; i >= 0; i--)
	{
		if (path[i] == '.')
		{
			if (flag == 1)
				return i;
			else
				flag = 1;
		}
	}
	return strlen(path);
}

int indexSetelahSlash(char *path, int hasil)
{
	for (int i = 0; i < strlen(path); i++)
	{
		if (path[i] == '/')
			return i + 1;
	}
	return hasil;
}

void enkripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, 0);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}

void dekripsiAwalan1(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, ".."))
		return;
	int idAkhir = indexTanpaExt(path);
	int idAwal = indexSetelahSlash(path, idAkhir);

	for (int i = idAwal; i < idAkhir; i++)
	{
		if (path[i] != '/' && isalpha(path[i]))
		{
			char karakter = path[i];
			if (isupper(path[i]))
			{
				karakter -= 'A';
				karakter = 25 - karakter;
			}
			else
			{
				karakter -= 'a';
				karakter = (13 + karakter) % 26;
			}

			if (isupper(path[i]))
				karakter += 'A';
			else
				karakter += 'a';

			path[i] = karakter;
		}
	}
}







// vigenere algorithm
char keyVigen[100] = "INNUGANTENG";
char *vigen(char str[])
{
	int strLen = strlen(str), i, j, keyLen = strlen(keyVigen);
	char newKey[strLen];

	// generating new key
	for (i = 0, j = 0; i < strLen; ++i, ++j)
	{
		if (j == keyLen)
			j = 0;

		newKey[i] = keyVigen[j];
	}
	newKey[i] = '\0';

	char encrypted[100];
	// encryption
	for (i = 0; i < strLen; ++i)
		encrypted[i] = ((str[i] + newKey[i]) % 26) + 'A';

	encrypted[i] = '\0';

	char *res = encrypted;

	return res;
}

char *find_path(const char *path)
{
	char fpath[2 * maxSize];
	bool flag, toMirror = 0;

	char *strMir;
	if (strcmp(path, "/")) // if path > /, mean if path contains /
	{
		strMir = strstr(path, "/IAN_");
		if (strMir)
		{
			// printf("Strmir past: %s \n", strMir);
			toMirror = 1;
			strMir++;
			// printf("Strmir new: %s \n", strMir);
		}
	}

	if (strcmp(path, "/") == 0)
	{
		sprintf(fpath, "%s", dirpath);
	}
	else if (toMirror)
	{
		char pathOrigin[maxSize] = "";
		char t[maxSize];

		strncpy(pathOrigin, path, strlen(path) - strlen(strMir)); // path origin only contain /
		// printf("Path origin: %s, path: %s \n", pathOrigin, path);
		strcpy(t, strMir);

		char *selectedFile;
		char *rest = t;

		flag = 0;

		while ((selectedFile = strtok_r(rest, "/", &rest)))
		{
			if (!flag)
			{
				strcat(pathOrigin, selectedFile);
				flag = 1;
				continue;
			}

			// printf("Path origin: %s, selectedFile: %s \n", pathOrigin, selectedFile);

			char checkType[2 * maxSize];
			sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
			strcat(pathOrigin, "/");
			// printf("pathOrigin: %s \n", pathOrigin);

			if (strlen(checkType) == strlen(path))
			{
				char pathFolder[2 * maxSize];
				sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

				DIR *dp = opendir(pathFolder);
				if (!dp)
				{
					char *ext;
					ext = strrchr(selectedFile, '.');

					char fileName[maxSize] = "";
					if (ext)
					{
						strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
						sprintf(fileName, "%s%s", vigen(fileName), ext);
					}
					else if (toMirror)
						strcpy(fileName, vigen(selectedFile));

					// printf("%s\n", fileName);
					strcat(pathOrigin, fileName);
				}
				else
					strcat(pathOrigin, vigen(selectedFile));
			}
			else
			{
				strcat(pathOrigin, vigen(selectedFile));
			}
		}
		sprintf(fpath, "%s%s", dirpath, pathOrigin);
	}
	else
		sprintf(fpath, "%s%s", dirpath, path);

	char *fpath_to_return = fpath;
	// printf("fpath_to_return: %s \n", fpath_to_return);
	return fpath_to_return;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
	char fpath[maxSize];
	bool toMirror = strstr(path, "/IAN_");
	strcpy(fpath, find_path(path));

	int res = 0;
	DIR *dp;
	struct dirent *de;

	(void)offset;
	(void)fi;

	dp = opendir(fpath);
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL)
	{
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
		{
			res = (filler(buf, de->d_name, &st, 0));
		}
		else if (toMirror)
		{
			if (de->d_type & DT_DIR)
			{
				char temp[maxSize];
				strcpy(temp, de->d_name);

				res = (filler(buf, vigen(temp), &st, 0));
			}
			else
			{
				char *ext;
				ext = strrchr(de->d_name, '.');

				char fileName[maxSize] = "";
				if (ext)
				{
					strncpy(fileName, de->d_name, strlen(de->d_name) - strlen(ext));
					strcpy(fileName, vigen(fileName));
					strcat(fileName, ext);
				}
				else
				{
					strcpy(fileName, vigen(de->d_name));
				}
				res = (filler(buf, fileName, &st, 0));
			}
		}
		else
			res = (filler(buf, de->d_name, &st, 0));

		if (res != 0)
			break;
	}
	closedir(dp);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	char fpath[maxSize];
	strcpy(fpath, find_path(path));

	int fd;
	int res;

	(void)fi;
	fd = open(fpath, O_RDONLY);
	if (fd == -1)
		return -errno;

	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;

	close(fd);
	return res;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
	char fpath[maxSize];
	strcpy(fpath, find_path(path));

	int res;
	res = lstat(fpath, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to){
	int result;
	char oldDir[1000], newDir[1000];
	
	char *a = strstr(to, awalan1);
	if (a != NULL) dekripsiAwalan1(a);

	sprintf(oldDir, "%s%s", dirPath, from);
	sprintf(newDir, "%s%s", dirPath, to);

	result = rename(oldDir, newDir);
	if (result == -1) return -errno;

	writeTheLog("RENAME", oldDir, newDir);

	return 0;
}


static struct fuse_operations operations = {
		// .getattr = do_getattr,
		// .mkdir = do_mkdir,
		// komen aja

		.getattr = xmp_getattr,
		.readdir = xmp_readdir,
		.read = xmp_read,
		.rename = xmp_rename,
};

int main(int argc, char *argv[])
{
	char *username = getenv("USER");

	sprintf(dirpath, "/home/%s", username);

	umask(0);

	return fuse_main(argc, argv, &operations, NULL);
}
